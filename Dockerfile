FROM tomcat:9


#DOCKERHUB_CREDENTIALS="hbdocker"
#DOCKER_IMAGE="hrishikesh6080/mvnimg1"

RUN rm -rf /usr/local/tomcat/webapps/*

COPY target/*.war /usr/local/tomcat/webapps/ROOT.war


EXPOSE 8080

CMD ["catalina.sh", "run"]
